from django.contrib import admin
from django.conf.urls import include, url

from project.apps.index.views import IndexView
from project.apps.jobs import views as job_views
from project.apps.users import views as user_views


urlpatterns = [
    # main urls
    url(r'^$', IndexView.as_view(), name='index'),

    # user urls
    url(r'^login/$', user_views.LoginView.as_view(), name='login'),
    url(r'^logout/$', user_views.LogoutView.as_view(), name='logout'),
    url(r'^signup/$', user_views.SignupView.as_view(), name='signup'),

    # job urls
    url(r'^jobs/$', job_views.JobListView.as_view(), name='job_list'),
    url(r'^job/(?P<pk>\d+)/$', job_views.JobView.as_view(), name='job'),
    url(r'^yoursearch/$', job_views.UpworkJobSearchView.as_view(), name='yoursearch'),
    url(r'^yoursearch/create/$', job_views.UpworkJobSearchCreateView.as_view(), name='yoursearch_create'),

    url(r'^admin/', include(admin.site.urls)),
]
