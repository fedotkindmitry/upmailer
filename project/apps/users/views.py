from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from django.core.urlresolvers import reverse_lazy
from django.views.generic import FormView, RedirectView
from django.contrib.auth.forms import AuthenticationForm


from forms import RegistrationForm


class SignupView(FormView):
    template_name = "apps/users/login.html"
    form_class = RegistrationForm

    def form_valid(self, form):
        User.objects.create_user(username=form.cleaned_data['username'],
                                 password=form.cleaned_data['password1'],
                                 email=form.cleaned_data['email'])
        return redirect('index')


class LoginView(FormView):
    success_url = reverse_lazy('index')
    form_class = AuthenticationForm
    template_name = "apps/users/login.html"

    def form_valid(self, form):
        login(self.request, form.get_user())
        return super(LoginView, self).form_valid(form)


class LogoutView(RedirectView):
    url = reverse_lazy('index')

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)
