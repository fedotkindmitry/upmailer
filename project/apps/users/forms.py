from django import forms


class RegistrationForm(forms.Form):
    username = forms.CharField(label="Username")
    email = forms.CharField(label="Email")
    password1 = forms.CharField(label="New password", widget=forms.PasswordInput())
    password2 = forms.CharField(label="New password again", widget=forms.PasswordInput())

    # def __init__(self, *args, **kwargs):
    #     self.request = kwargs.pop('request', None)
    #     super(RegistrationForm, self).__init__(*args, **kwargs)

    def clean(self):
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError("passwords dont match each other")
        return self.cleaned_data
