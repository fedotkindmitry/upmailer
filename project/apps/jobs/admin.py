from django.contrib import admin

from models import UpworkJobSearch, Job


admin.site.register(UpworkJobSearch)
admin.site.register(Job)
