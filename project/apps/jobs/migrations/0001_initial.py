# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('budget', models.IntegerField()),
                ('category', models.CharField(max_length=255)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('duration', models.CharField(max_length=255)),
                ('job_id', models.CharField(max_length=255)),
                ('job_status', models.CharField(max_length=255)),
                ('job_type', models.CharField(max_length=255)),
                ('skills', models.CharField(max_length=255)),
                ('snippet', models.TextField()),
                ('subcategory', models.CharField(max_length=255)),
                ('title', models.CharField(max_length=255)),
                ('workload', models.CharField(max_length=255)),
            ],
        ),
    ]
