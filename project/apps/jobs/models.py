from django.db import models
from solo.models import SingletonModel
from django.contrib.auth.models import User


# TODO choices
class Job(models.Model):
    budget = models.IntegerField(blank=True,
                                 null=True)

    category = models.CharField(max_length=255,
                                blank=True,
                                null=True)

    date_created = models.DateTimeField(auto_now_add=True,
                                        blank=True,
                                        null=True)

    duration = models.CharField(max_length=255,
                                blank=True,
                                null=True)

    job_id = models.CharField(max_length=255)

    job_status = models.CharField(max_length=255,
                                  blank=True,
                                  null=True)

    job_type = models.CharField(max_length=255,
                                blank=True,
                                null=True)

    skills = models.CharField(max_length=255,
                              blank=True,
                              null=True)

    snippet = models.TextField(blank=True,
                               null=True)

    subcategory = models.CharField(max_length=255,
                                   blank=True,
                                   null=True)

    title = models.CharField(max_length=255)

    workload = models.CharField(max_length=255,
                                blank=True,
                                null=True)


# TODO: other fields of search
class UpworkJobSearch(models.Model):
    user = models.ForeignKey(User)

    query = models.CharField(max_length=255,
                             blank=True,
                             null=True)

    title = models.CharField(max_length=255,
                             blank=True,
                             null=True)

    skills = models.CharField(max_length=255,
                              blank=True,
                              null=True)

    job = models.ManyToManyField(Job)
