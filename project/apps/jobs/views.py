from django import http
from django.contrib.auth import get_user
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, DetailView, TemplateView, CreateView

from project.apps.jobs.models import Job, UpworkJobSearch


class JobListView(ListView):
    model = Job
    template_name = "apps/jobs/job_list.html"


class JobView(DetailView):
    model = Job
    template_name = "apps/jobs/job.html"


class UpworkJobSearchView(TemplateView):
    template_name = "apps/jobs/search_view.html"

    def get_context_data(self, **kwargs):
        context = super(UpworkJobSearchView, self).get_context_data()
        context['search'] = UpworkJobSearch.objects.filter(user=self.request.user).first()
        return context


class UpworkJobSearchCreateView(CreateView):
    template_name = "apps/jobs/create_search.html"
    model = UpworkJobSearch
    fields = ['query', 'title', 'skills']
    success_url = reverse_lazy('yoursearch')

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = get_user(self.request)
        obj.save()
        import pdb; pdb.set_trace()
        return http.HttpResponseRedirect(reverse_lazy('yoursearch'))  # TODO super?
